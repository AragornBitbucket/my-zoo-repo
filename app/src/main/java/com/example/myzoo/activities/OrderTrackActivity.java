package com.example.myzoo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.adapter.CategoriesAdapter;
import com.example.myzoo.adapter.TimelineAdapter;
import com.example.myzoo.model.CategoriesPojo;
import com.example.myzoo.model.TimelineModel;

import java.util.ArrayList;
import java.util.List;

public class OrderTrackActivity extends MainActivity {

    List<TimelineModel> detailsList = new ArrayList<>();
    TimelineAdapter timelineadapter;
    RecyclerView timelineRecycler;

    View view;
    RecyclerView rvRelatedPetPost,rvMostViewedPost;
    CategoriesAdapter categoriesAdapter;
    RelativeLayout showAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order_track);

        view = View.inflate(this, R.layout.activity_order_track, null);
        addContentView(view);

        timelineRecycler = findViewById(R.id.timelineRecycler);
        rvRelatedPetPost = findViewById(R.id.rvRelatedPetPost);
        rvMostViewedPost = findViewById(R.id.rvMostViewedPost);
        showAll = findViewById(R.id.showAll);
        populatetimeline();

        categoriesAdapter = new CategoriesAdapter(OrderTrackActivity.this, getCategories());
//        categoriesAdapter.setOnItemClickListener(this);
        rvRelatedPetPost.setLayoutManager(new LinearLayoutManager(OrderTrackActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rvRelatedPetPost.setHasFixedSize(true);
        rvRelatedPetPost.setAdapter(categoriesAdapter);

        categoriesAdapter = new CategoriesAdapter(OrderTrackActivity.this, getCategories());
//        categoriesAdapter.setOnItemClickListener(this);
        rvMostViewedPost.setLayoutManager(new LinearLayoutManager(OrderTrackActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rvMostViewedPost.setHasFixedSize(true);
        rvMostViewedPost.setAdapter(categoriesAdapter);

        showAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderTrackActivity.this, OrderReturnActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });

    }

    private List<CategoriesPojo> getCategories(){

        List<CategoriesPojo> categoriesPojoList = new ArrayList<>();
        for(int i=0;i<10;i++){
            CategoriesPojo categoriesPojo = new CategoriesPojo();
            categoriesPojo.setCategory_name("Cooking All Need");
            categoriesPojo.setItems("Attas & Flours, Dals, Sugar");
            categoriesPojo.setImage(R.drawable.demo_img);
            categoriesPojoList.add(categoriesPojo);
        }

        return categoriesPojoList;
    }

    public void populatetimeline() {

        TimelineModel tm = new TimelineModel("Placed","",1);
        detailsList.add(tm);
        TimelineModel tm1 = new TimelineModel("Confirmed","",1);
        detailsList.add(tm1);
        TimelineModel tm2 = new TimelineModel("Ready for dispatch","",0);
        detailsList.add(tm2);
        TimelineModel tm3 = new TimelineModel("Dispatched","",0);
        detailsList.add(tm3);
        TimelineModel tm4 = new TimelineModel("Delivered","",0);
        detailsList.add(tm4);

        timelineadapter = new TimelineAdapter(this, detailsList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        timelineRecycler.setLayoutManager(mLayoutManager);
        timelineRecycler.setAdapter(timelineadapter);
    }
}