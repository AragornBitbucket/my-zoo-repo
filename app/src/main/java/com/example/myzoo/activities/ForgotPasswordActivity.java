package com.example.myzoo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.myzoo.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    ImageView img_close;
    AppCompatButton bt_getOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        System.out.println("className: "+ getClass().getSimpleName());

        img_close = findViewById(R.id.img_close);
        bt_getOTP = findViewById(R.id.bt_getOTP);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bt_getOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ForgotPasswordActivity.this, OTPVerificationActivity.class));
                finish();
            }
        });
    }
}