package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.adapter.MyPager;
import com.example.myzoo.adapter.ReviewsAdapter;
import com.example.myzoo.model.CategoriesPojo;
//import com.taufiqrahman.reviewratings.BarLabels;
//import com.taufiqrahman.reviewratings.RatingReviews;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ServiceProviderActivity extends MainActivity {

    ReviewsAdapter reviewsAdapter;
    RecyclerView rvReviews;
    View view;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private ViewPager viewPager;
    private MyPager myPager;
    List<Integer> imagesListNew = new ArrayList<>();
    AppCompatButton btSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_service_provider);
        view = View.inflate(this, R.layout.activity_service_provider, null);
        addContentView(view);
        rvReviews = findViewById(R.id.rvReviews);
        viewPager = findViewById(R.id.view_pager);

        btSubmit = findViewById(R.id.btSubmit);

        reviewsAdapter = new ReviewsAdapter(ServiceProviderActivity.this, getCategories());
        rvReviews.setLayoutManager(new LinearLayoutManager(ServiceProviderActivity.this, LinearLayoutManager.VERTICAL, false));
        rvReviews.setHasFixedSize(true);
        rvReviews.setAdapter(reviewsAdapter);

        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);
        imagesListNew.add(R.drawable.pet_details);
        imagesListNew.add(R.drawable.pet_details2);

        setImagesAdapter(imagesListNew);

       /* RatingReviews ratingReviews = (RatingReviews) findViewById(R.id.rating_reviews);

        int colors[] = new int[]{
                Color.parseColor("#0e9d58"),
                Color.parseColor("#bfd047"),
                Color.parseColor("#ffc105"),
                Color.parseColor("#ef7e14"),
                Color.parseColor("#d36259")};

        int raters[] = new int[]{
                new Random().nextInt(100),
                new Random().nextInt(100),
                new Random().nextInt(100),
                new Random().nextInt(100),
                new Random().nextInt(100)
        };

        ratingReviews.createRatingBars(100, BarLabels.STYPE1, colors, raters);*/

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServiceProviderActivity.this, MyBiddingActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });

    }

    private List<CategoriesPojo> getCategories(){

        List<CategoriesPojo> categoriesPojoList = new ArrayList<>();
        for(int i=0;i<10;i++){
            CategoriesPojo categoriesPojo = new CategoriesPojo();
            categoriesPojo.setCategory_name("Cooking All Need");
            categoriesPojo.setItems("Attas & Flours, Dals, Sugar");
            categoriesPojo.setImage(R.drawable.demo_img);
            categoriesPojoList.add(categoriesPojo);
        }

        return categoriesPojoList;
    }

    private void setImagesAdapter(List<Integer> imagesListNew) {

        myPager = new MyPager(ServiceProviderActivity.this, imagesListNew);
//        myPager = new MyPager(ProducatDetailActivity.this,images);
        viewPager.setAdapter(myPager);


        dotscount = myPager.getCount();
        System.out.println("dotscount: " + dotscount);
        System.out.println("imageSize: " + imagesListNew.size());

        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}