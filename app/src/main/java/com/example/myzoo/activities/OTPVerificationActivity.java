package com.example.myzoo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.myzoo.R;

public class OTPVerificationActivity extends AppCompatActivity {
    AppCompatButton bt_verify;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_verification);
        bt_verify = findViewById(R.id.bt_verify);
        bt_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OTPVerificationActivity.this, ResetPasswordActivity.class));
                finish();
            }
        });
    }
}