package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.adapter.MybiddingAdapter;
import com.example.myzoo.adapter.ReviewsAdapter;
import com.example.myzoo.model.CategoriesPojo;
import com.example.myzoo.model.MyBiddingModel;

import java.util.ArrayList;
import java.util.List;

public class MyBiddingActivity extends MainActivity {

    View view;
    MybiddingAdapter mybiddingAdapter;
    RecyclerView rvBiddingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_my_bidding);
        view = View.inflate(this, R.layout.activity_my_bidding, null);
        addContentView(view);

        rvBiddingList = findViewById(R.id.rvBiddingList);

        mybiddingAdapter = new MybiddingAdapter(MyBiddingActivity.this, getCategories());
        rvBiddingList.setLayoutManager(new LinearLayoutManager(MyBiddingActivity.this, LinearLayoutManager.VERTICAL, false));
        rvBiddingList.setHasFixedSize(true);
        rvBiddingList.setAdapter(mybiddingAdapter);
    }

    private List<MyBiddingModel> getCategories(){

        List<MyBiddingModel> categoriesPojoList = new ArrayList<>();
        MyBiddingModel categoriesPojo1 = new MyBiddingModel("1");
        categoriesPojoList.add(categoriesPojo1);
        MyBiddingModel categoriesPojo2 = new MyBiddingModel("2");
        categoriesPojoList.add(categoriesPojo2);
        MyBiddingModel categoriesPojo3 = new MyBiddingModel("3");
        categoriesPojoList.add(categoriesPojo3);

        return categoriesPojoList;
    }
}