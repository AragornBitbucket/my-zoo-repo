package com.example.myzoo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myzoo.R;


public class OrderSuccessActivity extends AppCompatActivity {

    Button otpViewOrderDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success);

        otpViewOrderDetails = findViewById(R.id.otpViewOrderDetails);

        otpViewOrderDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderSuccessActivity.this, OrderTrackActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });
    }
}