package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.adapter.AllBidsAdapter;
import com.example.myzoo.adapter.MessageAdapter;

public class MessageActivity extends MainActivity {
    View view;
    RecyclerView recyclerView;
    MessageAdapter messageAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = View.inflate(this, R.layout.activity_message, null);
        addContentView(view);
        recyclerView = findViewById(R.id.recyclerView);
        messageAdapter = new MessageAdapter(MessageActivity.this);
//        allBidsAdapter.setOnItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(MessageActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(messageAdapter);
    }
}