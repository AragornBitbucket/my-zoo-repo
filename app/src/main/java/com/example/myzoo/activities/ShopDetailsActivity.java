package com.example.myzoo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.adapter.CategoriesAdapter;
import com.example.myzoo.adapter.MyPager;
import com.example.myzoo.adapter.ReviewsAdapter;
import com.example.myzoo.model.CategoriesPojo;

import java.util.ArrayList;
import java.util.List;

public class ShopDetailsActivity extends MainActivity {
    View view;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private ViewPager viewPager;
    private MyPager myPager;

    List<Integer> imagesListNew = new ArrayList<>();

    RelativeLayout rlAuction;

    RecyclerView rvRelatedPetPost,rvMostViewedPost;

    CategoriesAdapter categoriesAdapter;
    ReviewsAdapter reviewsAdapter;

    AppCompatButton btSubmit;
//    AppCompatImageView bidnow;
    RatingBar ratingStar;

    RecyclerView rvReviews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = View.inflate(this, R.layout.activity_shop_details, null);
        addContentView(view);
        rlAuction = findViewById(R.id.rlAuction);
        viewPager = findViewById(R.id.view_pager);
        rvRelatedPetPost = findViewById(R.id.rvRelatedPetPost);
        rvMostViewedPost = findViewById(R.id.rvMostViewedPost);
        ratingStar = findViewById(R.id.ratingStar);
        btSubmit = findViewById(R.id.btSubmit);
//        bidnow = findViewById(R.id.chat);
        rvReviews = findViewById(R.id.rvReviews);

        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);

        imagesListNew.add(R.drawable.shop);
        imagesListNew.add(R.drawable.shop);

        setImagesAdapter(imagesListNew);





        ratingStar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                ratingVal.setText("Current Rating  : "+String.valueOf(rating));

                Toast.makeText(getApplicationContext(),String.valueOf(rating),Toast.LENGTH_SHORT).show();

            }
        });


        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String rating=String.valueOf(ratingStar.getRating());
                Toast.makeText(getApplicationContext(), rating, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ShopDetailsActivity.this, PostNewItemActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });


        categoriesAdapter = new CategoriesAdapter(ShopDetailsActivity.this, getCategories());
//        categoriesAdapter.setOnItemClickListener(this);
        rvRelatedPetPost.setLayoutManager(new LinearLayoutManager(ShopDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rvRelatedPetPost.setHasFixedSize(true);
        rvRelatedPetPost.setAdapter(categoriesAdapter);

        categoriesAdapter = new CategoriesAdapter(ShopDetailsActivity.this, getCategories());
//        categoriesAdapter.setOnItemClickListener(this);
        rvMostViewedPost.setLayoutManager(new LinearLayoutManager(ShopDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rvMostViewedPost.setHasFixedSize(true);
        rvMostViewedPost.setAdapter(categoriesAdapter);


        reviewsAdapter = new ReviewsAdapter(ShopDetailsActivity.this, getCategories());
//        categoriesAdapter.setOnItemClickListener(this);
        rvReviews.setLayoutManager(new LinearLayoutManager(ShopDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
        rvReviews.setHasFixedSize(true);
        rvReviews.setAdapter(reviewsAdapter);

    }


    private List<CategoriesPojo> getCategories(){

        List<CategoriesPojo> categoriesPojoList = new ArrayList<>();
        for(int i=0;i<10;i++){
            CategoriesPojo categoriesPojo = new CategoriesPojo();
            categoriesPojo.setCategory_name("Cooking All Need");
            categoriesPojo.setItems("Attas & Flours, Dals, Sugar");
            categoriesPojo.setImage(R.drawable.demo_img);
            categoriesPojoList.add(categoriesPojo);
        }

        return categoriesPojoList;
    }


    private void setImagesAdapter(List<Integer> imagesListNew) {

        myPager = new MyPager(ShopDetailsActivity.this, imagesListNew);
//        myPager = new MyPager(ProducatDetailActivity.this,images);
        viewPager.setAdapter(myPager);


        dotscount = myPager.getCount();
        System.out.println("dotscount: " + dotscount);
        System.out.println("imageSize: " + imagesListNew.size());

        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}