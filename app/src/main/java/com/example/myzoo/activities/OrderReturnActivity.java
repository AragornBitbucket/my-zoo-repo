package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;

public class OrderReturnActivity  extends MainActivity {

    View view;
    Button bt_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order_return);

        view = View.inflate(this, R.layout.activity_order_return, null);
        addContentView(view);

        bt_return = findViewById(R.id.bt_return);

        bt_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderReturnActivity.this, OrderFeedbackActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });
    }
}