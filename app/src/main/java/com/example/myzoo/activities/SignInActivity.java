package com.example.myzoo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.SplashActivity;

public class SignInActivity extends AppCompatActivity {

    AppCompatButton bt_signIn;
    TextView tv_forgotPassword;
    LinearLayout layout_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        System.out.println("className: "+ getClass().getSimpleName());

        bt_signIn = findViewById(R.id.bt_signIn);
        tv_forgotPassword = findViewById(R.id.tv_forgotPassword);
        layout_signup = findViewById(R.id.layout_signup);
        bt_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                intent.putExtra("page", "fragment");
                startActivity(intent);
                finish();
            }
        });
        tv_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, ForgotPasswordActivity.class));
            }
        });
        layout_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
            }
        });

    }
}