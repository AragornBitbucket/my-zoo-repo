package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;

public class OrderFeedbackActivity extends MainActivity {

    View view;
    AppCompatButton btSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order_feedback);

        view = View.inflate(this, R.layout.activity_order_feedback, null);
        addContentView(view);

        btSubmit = findViewById(R.id.btSubmit);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderFeedbackActivity.this, ServiceProviderActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });

    }
}