package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.SplashActivity;
import com.example.myzoo.adapter.AllBidsAdapter;
import com.example.myzoo.adapter.CategoriesAdapter;
import com.example.myzoo.model.CategoriesPojo;

import java.util.ArrayList;
import java.util.List;

public class BidPage extends MainActivity {

    View view;
    RecyclerView rvBids;
    AllBidsAdapter allBidsAdapter;
    Button bt_placeBid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_bid_page);

        view = View.inflate(this, R.layout.activity_bid_page, null);
        addContentView(view);

        rvBids = findViewById(R.id.rvBids);
        bt_placeBid = findViewById(R.id.bt_placeBid);


        allBidsAdapter = new AllBidsAdapter(BidPage.this, getCategories());
//        allBidsAdapter.setOnItemClickListener(this);
        rvBids.setLayoutManager(new LinearLayoutManager(BidPage.this, LinearLayoutManager.VERTICAL, false));
        rvBids.setHasFixedSize(true);
        rvBids.setAdapter(allBidsAdapter);

        bt_placeBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(BidPage.this, PostNewItemActivity.class);
                Intent intent = new Intent(BidPage.this, SellerDetailsActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });
    }


    private List<CategoriesPojo> getCategories(){

        List<CategoriesPojo> categoriesPojoList = new ArrayList<>();
        {
            CategoriesPojo categoriesPojo = new CategoriesPojo();
            categoriesPojo.setCategory_name("Cooking All Need");
//                    categoriesPojo.setImageId(R.drawable.ataa);
            categoriesPojo.setItems("Attas & Flours, Dals, Sugar");

            categoriesPojoList.add(categoriesPojo);


        }

        return categoriesPojoList;
    }
}