package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;

public class PostNewItemActivity extends MainActivity {
    View view;
    AppCompatButton btPost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = View.inflate(this, R.layout.activity_post_new_item, null);
        addContentView(view);

        btPost = findViewById(R.id.btPost);
        btPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(PostNewItemActivity.this, ProfileActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });
    }
}