package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;
import com.example.myzoo.fragment.AccessoriesFragment;
import com.example.myzoo.fragment.AllFragment;
import com.example.myzoo.fragment.PetsFragment;
import com.example.myzoo.fragment.ServicesFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class BrowseCategoryActivity extends MainActivity {
    View view;
    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = View.inflate(this, R.layout.activity_browse_category, null);
        addContentView(view);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout =findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

//        layout_editprofile = findViewById(R.id.layout_editprofile);
//        layout_editprofile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(BrowseCategoryActivity.this, ContactUsActivity.class);
//                intent.putExtra("page","activity");
//                startActivity(intent);
//            }
//        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PetsFragment(), "Pets");
        adapter.addFragment(new AccessoriesFragment(), "Accessories");
        adapter.addFragment(new ServicesFragment(), "Services");
        adapter.addFragment(new AllFragment(), "All");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}