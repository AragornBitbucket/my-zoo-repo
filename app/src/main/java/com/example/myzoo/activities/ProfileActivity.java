package com.example.myzoo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.myzoo.MainActivity;
import com.example.myzoo.R;

public class ProfileActivity extends MainActivity {
    View view;
    LinearLayout layout_editprofile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = View.inflate(this, R.layout.activity_profile, null);
        addContentView(view);

        layout_editprofile = findViewById(R.id.layout_editprofile);
        layout_editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ContactUsActivity.class);
                intent.putExtra("page","activity");
                startActivity(intent);
            }
        });
    }
}