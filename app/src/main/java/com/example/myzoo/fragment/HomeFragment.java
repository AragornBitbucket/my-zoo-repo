package com.example.myzoo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myzoo.R;
import com.example.myzoo.activities.PetDetailsActivity1;
import com.example.myzoo.activities.PetDetailsActivity2;
import com.example.myzoo.adapter.CategoriesAdapter;
import com.example.myzoo.model.CategoriesPojo;
import com.example.myzoo.utility.MethodUtils;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements CategoriesAdapter.OnPetClickListeners {


    RecyclerView rvRecentPetPost,rvRecentAccPost;




    CategoriesAdapter categoriesAdapter;


    public HomeFragment() {
        // Required empty public constructor
    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        rvRecentPetPost = view.findViewById(R.id.rvRecentPetPost);
        rvRecentAccPost = view.findViewById(R.id.rvRecentAccPost);

        MethodUtils.section = "home";

        categoriesAdapter = new CategoriesAdapter(getActivity(), getCategories());
        categoriesAdapter.setOnItemClickListener(this);
        rvRecentPetPost.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvRecentPetPost.setHasFixedSize(true);
        rvRecentPetPost.setAdapter(categoriesAdapter);

        categoriesAdapter = new CategoriesAdapter(getActivity(), getAccessories());
        categoriesAdapter.setOnItemClickListener(this);
        rvRecentAccPost.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvRecentAccPost.setHasFixedSize(true);
        rvRecentAccPost.setAdapter(categoriesAdapter);


        return view;
    }




    private List<CategoriesPojo> getCategories(){

        List<CategoriesPojo> categoriesPojoList = new ArrayList<>();
        for(int i=0;i<10;i++){
            CategoriesPojo categoriesPojo = new CategoriesPojo();
            categoriesPojo.setCategory_name("Cooking All Need");
            categoriesPojo.setItems("Attas & Flours, Dals, Sugar");
            categoriesPojo.setImage(R.drawable.demo_img);
            categoriesPojoList.add(categoriesPojo);
        }

        return categoriesPojoList;
    }

    private List<CategoriesPojo> getAccessories(){

        List<CategoriesPojo> categoriesPojoList = new ArrayList<>();
        for(int i=0;i<10;i++){
            CategoriesPojo categoriesPojo = new CategoriesPojo();
            categoriesPojo.setCategory_name("Cooking All Need");
            categoriesPojo.setItems("Attas & Flours, Dals, Sugar");
            categoriesPojo.setImage(R.drawable.demo_image1);
            categoriesPojoList.add(categoriesPojo);
        }

        return categoriesPojoList;
    }


    @Override
    public void onResume() {
        super.onResume();
        MethodUtils.page_home = "home";
        MethodUtils.section = "home";
        System.out.println("resume: "+" home calling");
    }

    @Override
    public void onPetDetails(int position) {
        Intent intent = new Intent(getActivity(), PetDetailsActivity1.class);
        intent.putExtra("page","activity");
        startActivity(intent);
    }
}
