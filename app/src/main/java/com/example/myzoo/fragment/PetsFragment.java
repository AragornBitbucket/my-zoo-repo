package com.example.myzoo.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myzoo.R;
import com.example.myzoo.activities.MessageActivity;
import com.example.myzoo.adapter.MessageAdapter;
import com.example.myzoo.adapter.PetsAdapter;


public class PetsFragment extends Fragment {

    RecyclerView recyclerView;
    PetsAdapter petsAdapter;

    public PetsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pets, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        petsAdapter = new PetsAdapter(getActivity());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(petsAdapter);
        return view;
    }
}