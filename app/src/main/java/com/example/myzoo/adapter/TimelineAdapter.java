package com.example.myzoo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myzoo.R;
import com.example.myzoo.model.TimelineModel;
import com.example.myzoo.utility.VectorDrawableUtils;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;


public class TimelineAdapter extends  RecyclerView.Adapter<TimelineAdapter.TimelineViewHolder> {
    Context con; List<TimelineModel> tl;

    public TimelineAdapter(Context con, List<TimelineModel> tl)
    {
        this.con=con;
        this.tl=tl;
    }

    @Override
    public TimelineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timelineview,parent, false);
        return new TimelineViewHolder(view, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public void onBindViewHolder(TimelineViewHolder holder, int position) {
          holder.bind(holder,tl.get(position),tl.size(),position);
    }

    @Override
    public int getItemCount() {
        return tl.size();
    }

    public  class TimelineViewHolder extends RecyclerView.ViewHolder {
        public TimelineView mTimelineView;

        TextView tvstatus,tvdetails;

        public TimelineViewHolder(View itemView, int viewType) {
            super(itemView);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.timeline);
            tvstatus = itemView.findViewById(R.id.text_timeline_details);
            tvdetails = itemView.findViewById(R.id.text_timeline_status);
            mTimelineView.initLine(viewType);
        }

        public void bind(TimelineViewHolder holder, TimelineModel tlm,int size,int pos)
        {
            holder.tvstatus.setText(tlm.getOrderstattus()+"");
            holder.tvdetails.setText(tlm.getDetails()+"");

            if(tlm.getActive_status()==1)
            {
                setMarker(holder, R.drawable.button_icon, R.color.colorText1,0,size,pos);
            }
            else if(tlm.getActive_status()==2)
            {
                setMarker(holder, R.drawable.button_icon, R.color.colorText1,0,size,pos);
            }
            else
            {
                setMarker(holder, R.drawable.button_icon, R.color.colorLightTxtGrey,4,size,pos);
                holder.tvdetails.setTextColor(con.getResources().getColor(R.color.colorLightTxtGrey));
            }
        }

        public void setMarker(TimelineViewHolder holder,int drawableResId, int colorFilter,int gap,int size,int pos)
        {
            holder.mTimelineView.setLineStyleDashGap(gap);
            if(pos > 0) {
                holder.mTimelineView.setStartLineColor(ContextCompat.getColor(holder.itemView.getContext(), colorFilter), 0);
            }

            if(pos!=size-1)
            {
                holder.mTimelineView.setEndLineColor(ContextCompat.getColor(holder.itemView.getContext(),colorFilter),0);
            }

            holder.mTimelineView.setMarker(VectorDrawableUtils.INSTANCE.getDrawable(holder.itemView.getContext(),drawableResId, ContextCompat.getColor(holder.itemView.getContext(),colorFilter))); //VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId, ContextCompat.getColor(holder.itemView.getContext(), colorFilter))
        }


    }
}
