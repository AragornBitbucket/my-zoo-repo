package com.example.myzoo.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myzoo.R;
import com.example.myzoo.activities.BrowseCategoryActivity;
import com.example.myzoo.activities.OrderSuccessActivity;

public class PetsAdapter extends RecyclerView.Adapter<PetsAdapter.MyViewHolder> {

    Activity activity;

    public PetsAdapter(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_all_pets, parent, false);

        return new PetsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

//        holder.cardPet.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                try {
//                    if (itemClickListener!= null){
//
//                        itemClickListener.onPetDetails(position);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, OrderSuccessActivity.class);
                intent.putExtra("page","activity");
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

//        CardView cardPet;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

//            cardPet = itemView.findViewById(R.id.cardPet);
        }
    }

//    OnPetClickListeners itemClickListener;

    /*public void setOnItemClickListener(OnPetClickListeners itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface OnPetClickListeners {


        void onPetDetails(int position);


    }*/

}
