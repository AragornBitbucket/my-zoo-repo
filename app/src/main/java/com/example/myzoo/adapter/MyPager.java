package com.example.myzoo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.example.myzoo.R;

import java.util.List;

public class MyPager extends PagerAdapter {

    Activity activity;
    List<Integer> imagesList;
//    String[] imagesList;

    private Context context;
    private LayoutInflater layoutInflater;

//    public MyPager(Activity activity, List<String> imagesList) {
//
//        this.activity = activity;
//        this.imagesList = imagesList;
//    }

    public MyPager(Context context, List<Integer> imagesList) {

        this.context = context;
        this.imagesList = imagesList;
    }


//    @Override
//    public void destroyItem(ViewGroup container, int position, Object view) {
//        container.removeView((View) view);
//    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }



    @Override
    public int getCount() {
        return imagesList.size();
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.pager_item, null);
//        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        ImageView imageView = (ImageView) view.findViewById(R.id.ivPager);

        imageView.setImageResource(imagesList.get(position));

//        imageView.setImageResource(images[position]);


        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;



    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }




//    private int getImageAt(int position) {
//        switch (position) {
//            case 0:
//                return Integer.parseInt(imagesList.get(position).getImageName());
//            case 1:
//                return R.drawable.colosseum;
//            case 2:
//                return R.drawable.eiffel_tower;
//            case 3:
//                return R.drawable.statue_of_liberty;
//            default:
//                return R.drawable.india_taj_mahal;
//        }
//    }

}
