package com.example.myzoo.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myzoo.R;
import com.example.myzoo.model.MyBiddingModel;
import com.example.myzoo.widget.RobotoBoldTextView;
import com.example.myzoo.widget.RobotoRegularTextView;

import java.util.List;

public class MybiddingAdapter extends RecyclerView.Adapter<MybiddingAdapter.MyViewHolder> {

    Activity activity;
    List<MyBiddingModel> categoriesPojoList;

    public MybiddingAdapter(Activity activity, List<MyBiddingModel> categoriesPojoList) {
        this.activity = activity;
        this.categoriesPojoList = categoriesPojoList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mybidding_lists, parent, false);

        return new MybiddingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (categoriesPojoList.get(position).getBidType().equals("1"))
        {
            holder.layPlaceNewBid.setVisibility(View.GONE);
        }
        else if (categoriesPojoList.get(position).getBidType().equals("2"))
        {
            holder.layPlaceNewBid.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.layPlaceNewBid.setVisibility(View.GONE);
            holder.tvHighestBid.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return categoriesPojoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewBidType,imageViewProduct;
        RobotoBoldTextView tvPetName;
        RobotoRegularTextView tvPostedBy,tvRatingStar,tvMyBid,tvDate,tvHighestBid,tvNewBid;
        LinearLayout layPlaceNewBid;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewBidType = itemView.findViewById(R.id.imageViewBidType);
            imageViewProduct = itemView.findViewById(R.id.imageViewProduct);
            tvPetName = itemView.findViewById(R.id.tvPetName);
            tvPostedBy = itemView.findViewById(R.id.tvPostedBy);
            tvRatingStar = itemView.findViewById(R.id.tvRatingStar);
            tvMyBid = itemView.findViewById(R.id.tvMyBid);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvHighestBid = itemView.findViewById(R.id.tvHighestBid);
            tvNewBid = itemView.findViewById(R.id.tvNewBid);
            layPlaceNewBid = itemView.findViewById(R.id.layPlaceNewBid);
        }
    }

}
