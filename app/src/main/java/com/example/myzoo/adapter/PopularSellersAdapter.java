package com.example.myzoo.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myzoo.R;
import com.example.myzoo.model.CategoriesPojo;

import java.util.List;

public class PopularSellersAdapter extends RecyclerView.Adapter<PopularSellersAdapter.MyViewHolder> {

    Activity activity;
    List<CategoriesPojo> categoriesPojoList;

    public PopularSellersAdapter(Activity activity, List<CategoriesPojo> categoriesPojoList) {
        this.activity = activity;
        this.categoriesPojoList = categoriesPojoList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_sellers, parent, false);

        return new PopularSellersAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
//        holder.image.setImageResource(categoriesPojoList.get(position).getImage());
        holder.cardItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (itemClickListener!= null){

                        itemClickListener.onPetDetails(position);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoriesPojoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

//        RelativeLayout cardPet;
//        ImageView image;

        CardView cardItem;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cardItem = itemView.findViewById(R.id.cardItem);
//            image = itemView.findViewById(R.id.image);
        }
    }

    OnCardItemClickListners itemClickListener;

    public void setOnItemClickListener(OnCardItemClickListners itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface OnCardItemClickListners {


        void onPetDetails(int position);


    }

}
