package com.example.myzoo.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myzoo.R;
import com.example.myzoo.model.CategoriesPojo;

import java.util.List;

public class AllBidsAdapter extends RecyclerView.Adapter<AllBidsAdapter.MyViewHolder> {

    Activity activity;
    List<CategoriesPojo> categoriesPojoList;

    public AllBidsAdapter(Activity activity, List<CategoriesPojo> categoriesPojoList) {
        this.activity = activity;
        this.categoriesPojoList = categoriesPojoList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_all_bids, parent, false);

        return new AllBidsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

//        holder.cardPet.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                try {
//                    if (itemClickListener!= null){
//
//                        itemClickListener.onPetDetails(position);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

//        CardView cardPet;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

//            cardPet = itemView.findViewById(R.id.cardPet);
        }
    }

//    OnPetClickListeners itemClickListener;

    /*public void setOnItemClickListener(OnPetClickListeners itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface OnPetClickListeners {


        void onPetDetails(int position);


    }*/

}
