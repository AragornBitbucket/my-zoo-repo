package com.example.myzoo;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.myzoo.activities.OrderSuccessActivity;
import com.example.myzoo.fragment.HomeFragment;
import com.example.myzoo.utility.Constant;
import com.example.myzoo.utility.MethodUtils;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import static androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FrameLayout frameLayoutContent;

    public RelativeLayout rl_menu;

    public BottomNavigationView bottom_navigation;

    private ActionBarDrawerToggle mDrawerToggle;
    public DrawerLayout mDrawerLayout;

    public ImageButton img_topbar_menu;

    public ImageView iv_cross;

    public LinearLayout myOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDrawerLayout = findViewById(R.id.drawer_layout);


        frameLayoutContent = findViewById(R.id.frameLayoutContent);

        bottom_navigation = findViewById(R.id.bottom_navigation);

        rl_menu = findViewById(R.id.rl_menu);

        img_topbar_menu = findViewById(R.id.img_topbar_menu);
        iv_cross = findViewById(R.id.iv_cross);

        myOrder = findViewById(R.id.myOrder);



        initializeDrawer();


        String page = getIntent().getStringExtra("page");
        if (page.equalsIgnoreCase("fragment")) {
            openInitialFragment();

        } else {
            System.out.println("checkpage: " + "activity");



        }


        clickEvent();
    }

    private void openInitialFragment() {
//        openFragment(new HomeFragment(), Constant.HOME_TAG);
        MethodUtils.page_home = "page_home";
        rl_menu.setVisibility(View.VISIBLE);
//        rlBackHeader.setVisibility(View.GONE);
//        ivMyCart.setVisibility(View.GONE);
        viewFragment(new HomeFragment(), Constant.HOME_TAG);

    }

    private void viewFragment(Fragment fragment, String name) {
        //final FragmentManager fragmentManager = getFragmentManager();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutContent, fragment);
        // 1. Know how many fragments there are in the stack
        final int count = fragmentManager.getBackStackEntryCount();
        // 2. If the fragment is **not** "home type", save it to the stack
        if (name.equals("FRAGMENT_OTHER")) {
            fragmentTransaction.addToBackStack(name);
        }
        // Commit !
        fragmentTransaction.commit();
        // 3. After the commit, if the fragment is not an "home type" the back stack is changed, triggering the
        // OnBackStackChanged callback
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                // If the stack decreases it means I clicked the back button
                if (fragmentManager.getBackStackEntryCount() <= count) {
                    // pop all the fragment and remove the listener
                    fragmentManager.popBackStack("FRAGMENT_OTHER", POP_BACK_STACK_INCLUSIVE);
                    fragmentManager.removeOnBackStackChangedListener(this);
                    // set the home button selected
                    bottom_navigation.getMenu().getItem(0).setChecked(true);
                }
            }
        });

//        Toast.makeText(getApplicationContext(),getSupportFragmentManager().getBackStackEntryCount(),Toast.LENGTH_LONG).show();
    }




    private void clickEvent() {
        img_topbar_menu.setOnClickListener(this);
        iv_cross.setOnClickListener(this);
        myOrder.setOnClickListener(this);

    }


    private void initializeDrawer() {
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.app_name, R.string.app_name
        ) {
            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        initializeDrawerToggle(mDrawerToggle);
    }

    public void initializeDrawerToggle(ActionBarDrawerToggle mDrawerToggle) {
        this.mDrawerToggle = mDrawerToggle;
    }

    /**
     * To check whether the side menu is open or not
     */
    private boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_topbar_menu:
                if (isDrawerOpen())
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
                break;

            case R.id.iv_cross:
                if (isDrawerOpen()) {
                    mDrawerLayout.closeDrawers();
                }
                break;
            case R.id.myOrder:
                    Intent intent = new Intent(MainActivity.this, OrderSuccessActivity.class);
                    startActivity(intent);
                break;


        }

    }


    public void addContentView(View view) {
        frameLayoutContent.removeAllViews();
        frameLayoutContent.addView(view,
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
    }
}