package com.example.myzoo.model;

public class MyBiddingModel {

    String petName="",postedBy="",ratingStar="",myBid="",date="",highestBid="",bidType="";
    String items="";
    int image;

    MyBiddingModel(String petName,String postedBy,String ratingStar,String myBid,String date,String highestBid,String bidType)
    {
        this.petName = petName;
        this.postedBy = postedBy;
        this.ratingStar = ratingStar;
        this.myBid = myBid;
        this.date = date;
        this.highestBid = highestBid;
        this.bidType = bidType;
    }

    public MyBiddingModel(String type)
    {
        this.bidType = type;
    }


    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public String getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(String ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getMyBid() {
        return myBid;
    }

    public void setMyBid(String myBid) {
        this.myBid = myBid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHighestBid() {
        return highestBid;
    }

    public void setHighestBid(String highestBid) {
        this.highestBid = highestBid;
    }

    public String getBidType() {
        return bidType;
    }

    public void setBidType(String bidType) {
        this.bidType = bidType;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
