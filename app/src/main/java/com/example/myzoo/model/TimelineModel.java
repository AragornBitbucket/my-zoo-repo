package com.example.myzoo.model;

public class TimelineModel {
    String details;
    String orderstattus;
    int active_status;

    public TimelineModel(String details, String orderstattus, int active_status) {
        this.details = details;
        this.orderstattus = orderstattus;
        this.active_status = active_status;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getOrderstattus() {
        return orderstattus;
    }

    public void setOrderstattus(String orderstattus) {
        this.orderstattus = orderstattus;
    }

    public int getActive_status() {
        return active_status;
    }

    public void setActive_status(int active_status) {
        this.active_status = active_status;
    }


}
